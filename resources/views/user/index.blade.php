@extends('layouts.principal')
    @section('content')

        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
            </tr>
            </thead>
            <tbody>
            @foreach($datos as $dato)
            <tr>
                <th scope="row"> {{$dato->id}}</th>
                <td>{{$dato->name}}</td>
                <td>{{$dato->email}}</td>
            </tr>

          @endforeach
            </tbody>
        </table>
        <a href="{{url('/excel')}}">Exportar</a>

    @endsection
