@extends('layouts.principal')
    @section('content')

        @forelse($datos as $libros)
            <li>{{ $libros->name }}  ,
                <a href="{{ route('book.show',['id' => $libros->id]) }}"> detalles </a> |
                <a href="{{ route('book.edit',['id' => $libros->id]) }}"> Editar </a> |

                <form method="post" action=" {{route('book.destroy',$libros)}}" >
                    {{ csrf_field() }}
                    {{method_field('DELETE')}}
                    <button type="submit" class="btn btn-link">Eliminar</button>
                </form>

            </li>

        @empty
            <li>No hay libros aun.</li>
        @endforelse
    @endsection
