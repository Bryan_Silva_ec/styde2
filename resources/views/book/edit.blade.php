@extends('layouts.principal')
@section('content')

    <h1>Editar usuarios</h1>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @php
                    $valores =0
                @endphp
                @foreach ($errors->all() as $error)

                    @php
                        $valores +=1
                    @endphp

                @endforeach
            </ul>
            <ul> Existen {{$valores}} errores en el formulario </ul>
        </div>
    @endif

    <form method="post" action="{{ url("book/$book->id") }}" >

        {{ csrf_field() }}
        {{method_field('PUT')}}

        <div class="form-group">
            <label for="name">Titulo del libro</label>
            <input type="text" class="form-control" id="name" name="name" placeholder="Agregué el titulo del libro" value="{{$book->name}}" >
            <small class="text-danger">{{ $errors->first('name') }} </small>
        </div>
        <div class="form-group">
            <label for="description">Descripción</label>
            <input type="text" class="form-control" id="description" name="description" placeholder="agregue una descripción" value="{{$book->description}}">
        </div>

        <div class="form-group">
            <label for="author_id">Autor</label>
            <input type="number" class="form-control" id="author_id" name="author_id"  placeholder="agregue el autor" value="{{$book->author_id}}">

        </div>
        <div class="form-group">
            <label for="house_id">Editorial</label>
            <input type="number" class="form-control" id="house_id" name="house_id" placeholder="agregue la editorial" value="{{$book->house_id}}">
        </div>

        <button type="submit" class="btn btn-primary">Actualizar usuarios</button>
    </form>


@endsection