@extends('layouts.principal')
@section('content')

  <h1>Creación de usuarios</h1>

  @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @php
                $valores =0
              @endphp
              @foreach ($errors->all() as $error)

                          @php
                              $valores +=1
                          @endphp

              @endforeach
          </ul>
          <ul> Existen {{$valores}} errores en el formulario </ul>
      </div>
  @endif

  <form method="post" action="{{ route('book.store') }}" >

      {{ csrf_field() }}
      <div class="form-group">
          <label for="name">Titulo del libro</label>
          <input type="text" class="form-control" id="name" name="name" placeholder="Agregué el titulo del libro" value="{{old('name')}}" >
          <small class="text-danger">{{ $errors->first('name') }} </small>
      </div>
      <div class="form-group">
          <label for="description">Descripción</label>
          <input type="text" class="form-control" id="description" name="description" placeholder="agregue una descripción" value="{{old('description')}}">
      </div>

      <div class="form-group">
          <label for="author_id">Autor</label>
          <input type="number" class="form-control" id="author_id" name="author_id"  placeholder="agregue el autor" value="{{old('author_id')}}"  >

      </div>
      <div class="form-group">
          <label for="house_id">Editorial</label>
          <input type="number" class="form-control" id="house_id" name="house_id" placeholder="agregue la editorial" value="{{old('house_id')}}">
      </div>

      <button type="submit" class="btn btn-primary">Enviar</button>
  </form>


@endsection