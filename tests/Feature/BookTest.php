<?php

namespace Tests\Feature;

use App\Http\Controllers\bookController;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Book;
use App\House;
use App\Author;

class BookTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return voresid
     */
    use RefreshDatabase;

    public function testExample()
    {
        $this->get('/book')->assertStatus(200);
        $this->assertTrue(true);
    }

    public function testShow() {

        Author::create([
            'name' => 'bryan'
        ]);

        house::create([
            'name' => 'nova casa editorial'
        ]);

        // creación de 50 usuarios
        factory(Author::class,50)->create();

        Book::create([
            'name' => 'Censurado',
            'description' => 'romance',
            'author_id' => 1,
            'house_id' => 1,
        ]);

        $this->get('/book/1')
            ->assertStatus(200);
        //$this->assertTrue(true);
    }

    public  function testStore() {

        $this->withExceptionHandling(); //ver las excepciones en las pruebas

        Author::create([
            'name' => 'bryan'
        ]);

        house::create([
            'name' => 'nova casa editorial'
        ]);


        $this->post('/book/',
           [
               'name' => 'Censurado plus',
               'description' => 'romance',
               'author_id' => 1,
               'house_id' => 1,
        ])->assertRedirect('/book');

        $this->assertDatabaseHas('books',
            [
                'name' => 'Censurado plus',
                'description' => 'romance',
                'author_id' => 1,
                'house_id' => 1,

            ]);
    }
    // prueba de nombre requerido
    /*
    public function testRequiredDescription() {

        $this->withExceptionHandling(); //ver las excepciones en las pruebas

        Author::create([
            'name' => 'bryan'
        ]);

        house::create([
            'name' => 'nova casa editorial'
        ]);



$this->from('book/create')
    ->post('/book/', [
        'name' => '',
        'description' => 'romance',
        'author_id' => 1,
        'house_id' => 1,
    ])
    ->assertRedirect('usuarios/nuevo')
    ->assertSessionHasErrors(['name']);

        /*->post('/book/',
            [
                'name' => 'censurado plus',
               // 'description' => 'romance',
                'author_id' => 1,
                'house_id' => 1,
            ])->assertRedirect('book')
            ->assertSessionHasErrors(['description']);
        //$this->assertDatabaseMissing('books',[ 'name' => 'censurado plus']);

    }
    */

}
