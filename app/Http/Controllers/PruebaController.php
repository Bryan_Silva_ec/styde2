<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PruebaController extends Controller
{
    // invoke para llamar a un solo metodo __invoke en la ruta solo el nombre del controlador
    //
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        $prueba = "Pagina de prueba";
        $nombres = [
            "tomas",
            "pedro"
        ];
        return view('prueba',compact('prueba','nombres'));
       //return view('prueba')->with('prueba',$prueba);
        //return view('prueba')->with('nombres',$nombres);
    }
}
