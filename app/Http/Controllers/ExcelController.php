<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InvoicesExport;
use Maatwebsite\Excel\Facades\Excel;
use App\User;

class ExcelController extends Controller
{

public function exportar() {

        return Excel::download(new InvoicesExport, 'Usuarios.xlsx');
}

}
