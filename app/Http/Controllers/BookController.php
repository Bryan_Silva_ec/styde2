<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Excel;


class bookController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // otro metodo para obtener datos ->$datos = DB::table('users')->get();
        // por medio de eloquent utilizando el metodo all() se recuperan todos los registros

        $datos = Book::all();

        return view('book.index',compact('datos')); // con compact paso los datos de los libros
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('book.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //


        $this->validate($request,
            ['name'=>'required|min:3|string|max:30|unique:books,name',
                'description'=>'required|string|min:5|max:30|',
                'author_id'=>'required|Numeric',
                'house_id'=>'required|Numeric'
            ],

            [   'name.required'=>'El nombre es obligatorio',
                'description.required'=>'La descripcion es obligatoria',
                'author_id.required' => 'eliga un autor ',
                'house_id.required' => 'eliga una editorial',

                'name.min'=>'el nombre debe tener mas de 3 caracteres',
                'description.min'=>'La descripcion debe tener mas de 5 caracteres',
                'author_id.Numeric' => 'el campo debe ser de tipo numerico',
                'house_id.Numeric' => 'el campo debe ser de tipo numerico',

                'name.max'=>'ha excedido el numero maximo de caracteres',
                'description.max'=>'ha excedido el numero maximo de caracteres',

                'name.unique'=>'El libro ya se encuentra registrado en la base de datos',

            ]);

        $data = $request->all();
        //dd($data);
        Book::create([
            'name' => $data['name'],
            'description' => $data['description'],
            'author_id' => $data['author_id'],
            'house_id' => $data['house_id'],
        ]);

        return redirect()->route('book.index');
        //return url('/book');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)

    {
        //$datos = Book::find($id);
        //$datos = Book::where("id","=",$id)->find(1);


            $datos = Book::findOrFail($id);

            /*
        if ($datos == null) {
            //return view('errors.404');

            return response()->view('errors.404', [], 404);
        }
        */
        //dd($datos);
        return view('book.show',compact('datos') );


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $book = Book::findOrFail($id);
        //dd($book);
        return view('book.edit',compact('book'));
        //return "procesando";
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        //
/*
        $this->validate($request,
            ['name'=>'required|min:3|string|max:30|unique:books,name',
                'description'=>'required|string|min:5|max:30|',
                'author_id'=>'required|Numeric',
                'house_id'=>'required|Numeric'
            ],

            [   'name.required'=>'El nombre es obligatorio',
                'description.required'=>'La descripcion es obligatoria',
                'author_id.required' => 'eliga un autor ',
                'house_id.required' => 'eliga una editorial',

                'name.min'=>'el nombre debe tener mas de 3 caracteres',
                'description.min'=>'La descripcion debe tener mas de 5 caracteres',
                'author_id.Numeric' => 'el campo debe ser de tipo numerico',
                'house_id.Numeric' => 'el campo debe ser de tipo numerico',

                'name.max'=>'ha excedido el numero maximo de caracteres',
                'description.max'=>'ha excedido el numero maximo de caracteres',

                'name.unique'=>'El libro ya se encuentra registrado en la base de datos',

            ]);

        */
$data = $request->all();



        $post = Book::findOrFail($id);
        //echo $post->name;
        //echo $request->name;
        //dd($data);

        $post->name = $request->name;
        $post->description = $request->description;
        $post->author_id = $request->author_id;
        $post->house_id = $request->house_id;

        $post->save();
        //Book::updated($data);

        /*
        Book::create([
            'name' => $data['name'],
            'description' => $data['description'],
            'author_id' => $data['author_id'],
            'house_id' => $data['house_id'],
        ]);*/

        return redirect()->route('book.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        $book->delete();
        return redirect()->route('book.index');
    }

    public function ExportarExcel() {

    }
}
