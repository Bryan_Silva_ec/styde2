<?php

namespace App;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class InvoicesExport implements  FromView //, FromCollection
{
    /* fromcollection
    public function collection() : view
    {
        //return User::all();
        $users = \App\User::all();
        return view('excel.User',compact('users'));
    }
    */

    public function view() : view
    {
        $users = \App\User::all();
        return view('excel.User',compact('users'));
    }

}
