<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class house extends Model
{
    //
    protected $fillable = [
        'name',
    ];

    public function books() {
        $this->hasMany(Book::class);
    }
}
