<?php

use Illuminate\Database\Seeder;
use App\house;

class HouseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        house::create([
            'name' => 'nova casa editorial'
        ]);

    }
}
