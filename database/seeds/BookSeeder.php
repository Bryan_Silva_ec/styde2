<?php

use Illuminate\Database\Seeder;
use App\Book;

class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Book::create([
            'name' => 'Censurado',
            'description' => 'romance',
            'author_id' => 1,
            'house_id' => 1,
        ]);
    }
}
