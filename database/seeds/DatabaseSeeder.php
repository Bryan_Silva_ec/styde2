<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');
       /* DB::table('roles')->truncate();
        DB::table('users')->truncate();
        DB::table('authors')->truncate();
        DB::table('houses')->truncate();
        DB::table('books')->truncate(); */


        DB::statement('SET FOREIGN_KEY_CHECKS = 1;'); // Reactivamos la revisión de claves foráneas
        // $this->call(UsersTableSeeder::class);
        $this->call(rolesSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(AuthorsSedders::class);
        $this->call(HouseSeeder::class);
        $this->call(BookSeeder::class);
    }

}
