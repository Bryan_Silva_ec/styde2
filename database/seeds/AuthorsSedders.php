<?php

use Illuminate\Database\Seeder;
use App\Author;

class AuthorsSedders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // creación de un usuario
        Author::create([
            'name' => 'bryan'
        ]);

        // creación de 50 usuarios
        factory(Author::class,50)->create();
    }
}
