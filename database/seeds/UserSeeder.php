<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'bryan silva',
            'email' => 'bryansilva021@hotmail.com',
            'password' => encrypt('laravel'),
            'rol_id' => '1',

        ]);

        factory(User::class)->create([
           'rol_id' => 1
        ]);

        //factory(User::class,50)->create(); // crear 50 usuarios
    }
}
